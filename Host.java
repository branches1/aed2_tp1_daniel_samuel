
public class Host {
    private int info; //atributo para o número do bloco
    private Host proximo; //atributo da proximo referencia de bloco da lista
    
    public Host(int num) {
        this.info = num;
	}
	
	//GETS E SETS
	public int getInfo() {
        return this.info;
    }

    public void setInfo(int info) {
        this.info = info;
    }

    public Host getProximo() {
        return this.proximo;
    }

    public void setProximo(Host proximo) {
        this.proximo = proximo;
    }   

}
