
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;


public class Main {
    public static void main(String[] args)throws Exception{
        FileReader fr = null;
        BufferedReader br = null;
        String arquivo = "entrada.txt"; //variavel para receber o arquivo
        
        try {
            fr = new FileReader(arquivo); //lendo arquivo
            br = new BufferedReader(fr); //lendo linhas
            String linha = new String(br.readLine());
            String dados[]=linha.split(" ");
            
            Blocos Tblocos = new Blocos(Integer.parseInt(dados[0]));
            boolean parada = false; //bolean para ser verificado no case "quit"
            
            linha = br.readLine(); //variavel para receber a leitura das linhas do arquivo
            
            while(linha != null && parada==false){// para verificar enquanto tiver linha para ler e não for a linha "quit"
                String msg[]=linha.split(" ");
                int a, b;
                switch(msg[0]){ // case para verificar se é move ou pile
                    case "move": 
                        a = Integer.parseInt(msg[1]);
                        b = Integer.parseInt(msg[3]);
                        switch(msg[2]){ // case para verificar se é onto ou over
                            case"onto":
                                System.out.println(Tblocos.moveOnto(a, b));   
                            break;
                            case"over":
                                System.out.println(Tblocos.moveOver(a, b));
                            break;
                        }
                    break;
                    case "pile":
                        a = Integer.parseInt(msg[1]);
                        b = Integer.parseInt(msg[3]);
                        switch(msg[2]){
                            case"onto":
                                System.out.println(Tblocos.pileOnto(a, b));  
                            break;
                            case"over":
                                System.out.println(Tblocos.pileOver(a, b));      
                            break;
                        }                   
                    break;
                    case "quit":
						// variavel para escrever no arquivo de saida
                        FileWriter saida = new FileWriter ("saida.txt");
                        saida.write(Tblocos.PrintCompleto());
                        saida.close();
						parada = true;
                    break;
                }
                linha = br.readLine();
            }
             linha = br.readLine();
             System.out.println(Tblocos.PrintCompleto()); 
            
        }
        catch (Exception e) {
            System.out.println(" Erro no Arquivo .txt "+e);
        }
        
    }
}
