public class Blocos {
    // REGRAS DE NEGOCIO 
    
   private Host iniciandoLista;
   private Host finalLista;
   private int quantHost = 0;
   private Blocos blocos[];

    public Blocos() {
    }   

    // GET e SET
    // Trabalhando a lista encadeada

    public int getQuantidadeHost(){
        return quantHost;
    }

    public Host getIniciandoLista(){
        return this.iniciandoLista;
    }

    public Host getFianlLista(){
        return this.finalLista;        
    }

    private void setQuantidadeHost(int quantidade){
        if(quantidade >=0){
            quantHost = quantidade;            
        }else {
            quantHost = 0;
        }
    }

    private void setIniciandoLista( Host ILista){
        this.iniciandoLista = ILista;
    }

    private void setFinalLista(Host FLista){
        this.finalLista = FLista;
    }
    // Método para verificar se a lista está vazia
    public boolean caixa(){
        return this.getQuantidadeHost() == 0;
    }

     //passando tamanho da lista
     public Blocos(int q){
        this.blocos = new Blocos[q]; 
        for(int i = 0; i < q ; i++){
            blocos[i] = new Blocos();
            blocos[i].ADD(0,i);
        }
    }
    // Inicializar lista
    private void iniciandoLista() {
        this.setFinalLista(null); // vazio
        this.setIniciandoLista(null); // vazio
        this.setQuantidadeHost(0); 
    }
    //Método para inserir um nó na lista
    private boolean ADD (int a, int num){
            if((num<0) || (a>0) || (a>this.getQuantidadeHost())){ // Se posição negativa ou maior que a quantidade de nós
                return false;
        }else{
            Host noaux = new Host(num); //Cria-se um novo no auxiliar
            if(caixa()){ //Se a lista for vazia
                noaux.setProximo(null); //registra null como próximo
                setFinalLista(noaux); //descritor recebe ultimo nó
                setIniciandoLista(noaux); //descritor recebe o primeiro nó
            }else if (a == 0){
                noaux.setProximo(getIniciandoLista()); //primeiro nó da lista é copiado para o próximo do nó auxiliar 
                setIniciandoLista(noaux);
            }else if (a < getQuantidadeHost()){ //posição < ultima posição válida nó
                Host segundoAux = getIniciandoLista(); //inicia um novo nó
                for(int i = 0; i < a -1; i++){ //percorrer até antes da posição que quer inserir
                    segundoAux = segundoAux.getProximo(); 
                }
                noaux.setProximo(segundoAux);//registra o endereço do nó que está na posição 'a'
                segundoAux.setProximo(noaux); //segundo nó auxiliar regista o endereço do novo nó
            }else {
                noaux.setProximo(null);
                noaux.setInfo(num);
                getFianlLista().setProximo(noaux);
                setFinalLista(noaux);
            }
        }
        setQuantidadeHost(getQuantidadeHost() + 1); //atualiza quantidade de nós
        return true;
    }

    // Mostrando lista na tela
    public String Print(){
        String separador = ": ";
        if(!caixa()){
            Host noaux = getIniciandoLista();
            while(noaux != null){
                separador += noaux.getInfo() + " ";
                noaux = noaux.getProximo();
            }
        }
        return separador;
    }

    public String PrintCompleto(){
        String separador = "";
        for(int i = 0; i < blocos.length; i++)
            separador += (i+blocos[i].Print()+"\n");
        return separador;
    }

    //Trabalhando a movimentação dos Blocos
    //método para verificar se posição está ocupada
    public void ocupado (Blocos b){
        if(b.getIniciandoLista() == null){
            return;
        }
        int caixa = b.getIniciandoLista().getInfo();
        if(blocos[caixa] != null)
          ocupado(blocos[caixa]);
          blocos[caixa].setIniciandoLista(b.getIniciandoLista());
          blocos[caixa].setQuantidadeHost(b.getQuantidadeHost());
          b.iniciandoLista();
    }
	
	  //metodo para retornar os blocos para a posição original
    private void retornaPosicao(Blocos Tbloco) {
        
        if(!Tbloco.caixa()){
            Host noaux = Tbloco.getIniciandoLista().getProximo();

            while(noaux != null){
                int caixa = noaux.getInfo();
                if(blocos[caixa] != null)
                  ocupado(blocos[caixa]);                
                  blocos[caixa].setIniciandoLista(noaux);
                  blocos[caixa].setFinalLista(noaux);
                  blocos[caixa].setQuantidadeHost(1);
                  noaux = noaux.getProximo();
                  blocos[caixa].getIniciandoLista().setProximo(null);
            }
            Tbloco.getIniciandoLista().setProximo(null);
            Tbloco.setFinalLista(Tbloco.getIniciandoLista());
            Tbloco.setQuantidadeHost(1);//
        }
    }

    //metodo para mover os blocos que foram passado como parametro
    private void mover(Blocos a, Blocos b){
        if (!a.caixa())
            a.getFianlLista().setProximo(b.getIniciandoLista());
        else
          a.setIniciandoLista(b.getIniciandoLista());        
			    a.setQuantidadeHost(b.getQuantidadeHost() + a.getQuantidadeHost());
			    a.setFinalLista(b.getFianlLista());
			    b.iniciandoLista();
    }
	
	
    public String moveOnto(int a, int b) {
        //retorna os blocos que estão sobre A e B para suas posições originais
		    retornaPosicao(blocos[a]);
        retornaPosicao(blocos[b]);
        mover(blocos[b],blocos[a]);//move o bloco A para cima do bloco B 
        return PrintCompleto();
    }

    public String moveOver(int a, int b){
        retornaPosicao(blocos[a]); //retorna os blocos que estão sobre A para sua posição original
        mover(blocos[b],blocos[a]);//move o bloco A para o topo do bloco B
        return PrintCompleto();
    }

    public String pileOnto(int a, int b){ 
        retornaPosicao(blocos[b]); //retorna quem estiver sobre bloco B para sua posição original
        mover(blocos[b],blocos[a]); //move o bloco A e toda a pilha para cima do bloco B
        return PrintCompleto();
    }

    public String pileOver(int a, int b){
        mover(blocos[b],blocos[a]); //move o bloco A e sua pilha para cima da pilha que estiver no bloco B
        return PrintCompleto();
    }

}
